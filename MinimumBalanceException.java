/*
 * MinimumBalanceException.java
 *
 * File:
 *	$Id: 
 *
 * Revisions:
 *	$Log: 
 *
 */

/**
 * An exception thrown when a CD Account is opened with less than the minimum balance ($500)
 * 
 * @author Mitchell Derby and Ian Perry
 *
 */
public class MinimumBalanceException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
}
