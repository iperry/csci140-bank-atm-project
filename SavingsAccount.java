/*
 * SavingsAccount.java
 *
 * File:
 *	$Id: 
 *
 * Revisions:
 *	$Log: 
 *
 */

/**
 * 
 * @author Mitchell Derby and Ian Perry
 *
 */
public class SavingsAccount extends Account{

	/**
	 * constructor for a bank account.
	 * 
	 * @param accountNumber - the unique ID of the account (4 or more digits).
	 * @param pin - the 4 digit security pin of the account.
	 * @param balance - the starting balance of the account (must be more than $500).
	 */
	public SavingsAccount(String accountNumber, String pin, double balance){
		
		super(accountNumber, pin, balance);
		accountType = "Savings Account";
	}

	/**
	 * Method for applying interest to a Savings account. The interest rate for Savings accounts is .5%/year.
	 */
	synchronized public double applyInterest(){
			
		//No interest is applied if the Savings account balance is less than the minimum balance ($200.00)
		if(Balance < 200.00)
			return 0;
			
		/**
		 * double of balance w/ interest.
		 */
		double tempBalance;
		
		/**
		 * integer of balance w/ interest.
		 */
		int unroundedBalance;
		
		/**
		 * difference between double and integer values of balance w/ interest. Used for rounding.
		 */
		double remainder; 
		
	
		tempBalance = (int)(Balance*100);
		
		//Gets the balance*100 with interest applied but without parts of cents.
		unroundedBalance = (int) (tempBalance*(1+.05/12));
				
		//Gets the whole balance*100 with interest applied.
		tempBalance = (tempBalance*(1+.05/12));
		
		//Gets the parts of cents from applying interest to see if the number needs rounding.
		remainder = tempBalance-unroundedBalance;
		
		//Checks if the balance needs to be rounded to the nearest cent. 
		if(remainder > .5)
			tempBalance = (unroundedBalance+1)/100;
		else
			tempBalance = (unroundedBalance)/100;
		
		double oldBalance = Balance;
		Balance = tempBalance;
				
		return Balance-oldBalance;
		
	}
	
	/**
	 * Method for applying a penalty to a Savings account if its balance 
	 * is below the minimum balance required for this specific account type ($200).
	 * 
	 * @return The dollar amount of the applied penalty.
	 */
	synchronized public double applyPenalty(){
		
		if(Balance > 200.00)
			return 0.00;
		else if(Balance < 200.00 && Balance > 10.00){
			Balance = Balance-10;
			return 10.00;
		}else if(Balance < 10.00){
			
			/**
			 * The original Balance. Used to return the penalty that was subtracted from the account.
			 */
			double originalBalance = Balance;
			
			/**
			 * double of balance w/ penalty.
			 */
			double tempBalance;
				
			/**
			 * integer of balance w/ penalty.
			 */
			int unroundedBalance;
				
			/**
			 * difference between double and integer values of balance w/ penalty. Used for rounding.
			 */
			double remainder; 
			
			//Gets the whole balance*100 with penalty applied.
			tempBalance = (Balance*100)*.9;
			
			//Gets the balance*100 with penalty applied but without parts of cents.
			unroundedBalance = (int) ((Balance*100)*.9);
			
			//Gets the parts of cents from applying penalty to see if the number needs rounding.
			remainder = tempBalance-unroundedBalance;
		
			//Checks if the balance needs to be rounded to the nearest cent. 
			if(remainder > .5){
				tempBalance = unroundedBalance+1;
			}else{
				tempBalance = unroundedBalance;
			}
			
			Balance = tempBalance;
			
			return originalBalance-Balance;
		}else
			return 0.00;
		
		
	}
	
}
