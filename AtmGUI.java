import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * The GUI for ATM. Communicates with the Bank model that is passed to it.
 * Is called from BankGUI.
 * 
 * @author itp7999, mxd5415
 */
@SuppressWarnings("serial")
public class AtmGUI extends JFrame {
	
	private Bank model;
	private LinkedList<Integer> inputBuffer;
	private int stage;
	private String id;
	private JPanel main;
	private String problem;
	private JFrame frame = this;
	
	/**
	 * Constructor for AtmGUI. Opens to welcome screen if no id supplied,
	 * otherwise opens to PIN screen.
	 * 
	 * @param model Bank model for reference.
	 * @param id The id of an account that will be used in this ATM
	 */
	AtmGUI(Bank model, String id) {
		this.model = model;
		this.id = id;
		inputBuffer = new LinkedList<Integer>();
		problem = "";
		if(id.length() == 4)
			stage = 1;
		else
			stage = 0;
		
		setLayout(new BorderLayout());
		main = new JPanel();
		main.setLayout(new BorderLayout());
		add(main, BorderLayout.CENTER);
		update();
		
		setSize(550, 400);
		setLocation(100, 100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * @author itp7999, mxd5415
	 * 
	 * Listener for all the buttons on the ATM screen.
	 */
	class ButtonListener implements ActionListener {
		/**
		 * Called whenever a button is pressed
		 */
		public void actionPerformed(ActionEvent e) {
			boolean digit=false, ok=false, clear=false, cancel=false;
			int d=0;
			for(int i = 0; i <= 9; i++) {
				if(e.getActionCommand().compareTo(String.valueOf(i)) == 0) {
					digit = true;
					d = i;
				}
			}
			if(e.getActionCommand().compareTo("Cancel") == 0)
				cancel = true;
			if(e.getActionCommand().compareTo("Close") == 0) {
				frame.dispose();
			}
			if(e.getActionCommand().compareTo("Clear") == 0)
				clear = true;
			if(e.getActionCommand().compareTo("OK") == 0)
				ok = true;
			switch(stage) {
				case 0:
					//account id screen
					if(digit) {
						inputBuffer.addLast(d);
						update();
					}
					else if(clear) {
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					else if(ok) {
						if(inputBuffer.size() >= 4) {
							StringBuilder s = new StringBuilder("");
							for(int i = 0; i <= 3; i++)
								s.append(inputBuffer.get(i).toString());
							String testId = s.toString();
							if(model.IdIsValid(testId)) {
								stage++;
								id = testId;
								problem = "";
								inputBuffer = new LinkedList<Integer>();
								update();
							}
							else {
								problem = "Invalid Account ID";
								inputBuffer = new LinkedList<Integer>();
								update();
							}
						}
						else {
							problem = "Invalid Account ID";
							inputBuffer = new LinkedList<Integer>();
							update();
						}
					}
					//cancel not valid
					break;
				case 1:
					//enter pin
					if(digit) {
						inputBuffer.addLast(d);
						update();
					}
					else if(clear) {
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					else if(ok) {
						if(inputBuffer.size() >= 4) {
							StringBuilder s = new StringBuilder("");
							for(int i = 0; i <= 3; i++)
								s.append(inputBuffer.get(i).toString());
							String pin = s.toString();
							if(model.PinIsValid(id, pin)) {
								stage++;
								problem = "";
								inputBuffer = new LinkedList<Integer>();
								update();
							}
							else {
								problem = "Invalid PIN";
								stage--;
								inputBuffer = new LinkedList<Integer>();
								update();
							}
						}
						else {
							problem = "Invalid PIN";
							stage--;
							inputBuffer = new LinkedList<Integer>();
							update();
						}
					}
					else if(cancel) {
						stage--;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
				case 2:
					//transaction menu
					if(digit) {
						inputBuffer.addLast(d);
						update();
					}
					else if(clear) {
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					else if(ok) {
						if(inputBuffer.size() >= 1) {
							int num = inputBuffer.get(0);
							if(num >= 1 && num <= 3) {
								if(num == 1)
									stage = 5;
								else if(num == 2)
									stage = 3;
								else if(num == 3)
									stage = 6;
								problem = "";
								inputBuffer = new LinkedList<Integer>();
								update();
							}
							else {
								problem = "Invalid Input";
								inputBuffer = new LinkedList<Integer>();
								update();
							}
						}
						else {
							problem = "Invalid Input";
							inputBuffer = new LinkedList<Integer>();
							update();
						}
					}
					else if(cancel) {
						stage = 0;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
				case 3:
					//enter deposit amount
					if(digit) {
						inputBuffer.addLast(d);
						update();
					}
					else if(clear) {
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					else if(ok) {
						if(inputBuffer.size() >= 1) {
							double num;
							int total = 0, c=0;
							for(int i = inputBuffer.size()-1; i >= 0; i--) {
								total += inputBuffer.get(c) * Math.pow(10, i);
								c++;
							}
							num = (double)total / 100.0;
							if(model.deposit(id, num)) {
								stage++;
								problem = "";
								inputBuffer = new LinkedList<Integer>();
								update();
							}
							else {
								problem = "Invalid Deposit";
								stage = 2;
								inputBuffer = new LinkedList<Integer>();
								update();
							}
						}
						else {
							problem = "Invalid Input";
							stage = 2;
							inputBuffer = new LinkedList<Integer>();
							update();
						}
					}
					else if(cancel) {
						stage = 2;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
				case 4:
					//successful deposit
					if(ok) {
						stage = 2;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
				case 5:
					//display balance
					if(ok) {
						stage = 2;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
				case 6:
					//enter withdraw amount
					if(digit) {
						inputBuffer.addLast(d);
						update();
					}
					else if(clear) {
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					else if(ok) {
						if(inputBuffer.size() >= 1) {
							double num;
							int total = 0, c=0;
							for(int i = inputBuffer.size()-1; i >= 0; i--) {
								total += inputBuffer.get(c) * Math.pow(10, i);
								c++;
							}
							num = (double)total / 100.0;
							if(model.withdraw(id, num)) {
								stage++;
								problem = "";
								inputBuffer = new LinkedList<Integer>();
								update();
							}
							else {
								problem = "";
								stage = 8;
								inputBuffer = new LinkedList<Integer>();
								update();
							}
						}
						else {
							problem = "";
							stage = 8;
							inputBuffer = new LinkedList<Integer>();
							update();
						}
					}
					else if(cancel) {
						stage = 2;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
				case 7:
					//successful withdraw
					if(ok) {
						stage = 2;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
				case 8:
					//invalid amount
					if(ok) {
						stage = 2;
						problem = "";
						inputBuffer = new LinkedList<Integer>();
						update();
					}
					break;
			}
		}
	}
	
	/**
	 * Puts together the GUI for each stage of the ATM menus
	 */
	private void update() {
		main.removeAll();
		if(stage > 0)
			setTitle("ACME ATM: " + id + " (Made by Ian Perry & Mitchell Derby)");
		else
			setTitle("ACME ATM (Made by Ian Perry & Mitchell Derby)");
		drawKeypad();
		
		JPanel screen = new JPanel();
		JPanel text;
		switch(stage) {
			case 0:
				//account id screen
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new BorderLayout());
				text.add(new JLabel("Welcome to the ATM"), BorderLayout.NORTH);
				text.add(new JLabel("Please enter account ID:"), BorderLayout.CENTER);
				screen.add(text, BorderLayout.NORTH);
				screen.add(displayInputCharacter(4, false), BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 1:
				//enter pin
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new BorderLayout());
				text.add(new JLabel("Welcome to the ATM Account " + id), BorderLayout.NORTH);
				text.add(new JLabel("Please enter your PIN:"), BorderLayout.CENTER);
				screen.add(text, BorderLayout.NORTH);
				screen.add(displayInputCharacter(4, true), BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 2:
				//transaction menu
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new GridLayout(4, 1));
				text.add(new JLabel("What would you like to do?"));
				text.add(new JLabel("1. Balance Inquiry"));
				text.add(new JLabel("2. Deposit"));
				text.add(new JLabel("3. Withdraw"));
				screen.add(text, BorderLayout.NORTH);
				screen.add(displayInputCharacter(1, false), BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 3:
				//enter deposit amount
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new GridLayout(1, 1));
				text.add(new JLabel("Please Enter Deposit Amount:"));
				screen.add(text, BorderLayout.NORTH);
				screen.add(displayAmountCharacters(), BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 4:
				//successful deposit
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new GridLayout(1, 1));
				text.add(new JLabel("Deposit Successful."));
				screen.add(text, BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 5:
				//display balance
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new GridLayout(2, 1));
				text.add(new JLabel("You're account balance is:"));
				text.add(new JLabel("$ " + model.getAccountBalance(id)));
				screen.add(text, BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 6:
				//enter withdraw amount
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new GridLayout(1, 1));
				text.add(new JLabel("Please Enter Withdraw Amount:"));
				screen.add(text, BorderLayout.NORTH);
				screen.add(displayAmountCharacters(), BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 7:
				//successful withdraw
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new GridLayout(1, 1));
				text.add(new JLabel("Withdraw Successful."));
				screen.add(text, BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
			case 8:
				//invalid amount
				screen.setLayout(new BorderLayout());
				text = new JPanel();
				text.setLayout(new GridLayout(1, 1));
				text.add(new JLabel("Withdraw Failure."));
				screen.add(text, BorderLayout.CENTER);
				screen.add(new JLabel(problem), BorderLayout.SOUTH);
				break;
		}
		main.add(screen, BorderLayout.CENTER);
		
		repaint();
		validate();
	}
	
	/**
	 * Shows in the proper format, the input string for entering dollar amounts
	 * @return a JPanel with a Label containing the properly formatted text
	 */
	private JPanel displayAmountCharacters() {
		JPanel p = new JPanel();
		p.setLayout(new BorderLayout());
			
		StringBuilder s = new StringBuilder("");
		if(inputBuffer.size() <= 4) {
			for(int i = 1; i <= 4 - inputBuffer.size(); i++) {
				if(s.length() == 2)
					s.append(".");
				s.append("0");
			}
			for(Integer i : inputBuffer) {
				if(s.length() == 2)
					s.append(".");
				s.append(i);
			}
		}
		else {
			for(Integer i : inputBuffer) {
				if(s.length() == inputBuffer.size() - 2)
					s.append(".");
				s.append(i);
			}
		}
		
		JLabel l = new JLabel(s.toString());
		p.add(l, BorderLayout.CENTER);
		return p;
	}

	/**
	 * Shows in the proper format, the input string for entering PINs and digits
	 * @param numChars either 4 or 1, depending on the use.
	 * @param hideChars hide character with asterisks for PIN
	 * @return a JPanel with a Label containing the properly formatted text
	 */
	private JPanel displayInputCharacter(int numChars, boolean hideChars) {
		JPanel p = new JPanel();
		p.setLayout(new BorderLayout());
		
		StringBuilder s = new StringBuilder("");
		for(Integer i : inputBuffer) {
			if(s.length() < numChars) {
				if (hideChars)
					s.append("*");
				else
					s.append(i);
			}
		}
		while(s.length() < numChars)
			s.append("-");
		
		JLabel l = new JLabel(s.toString());
		p.add(l, BorderLayout.CENTER);
		return p;
	}
	
	/**
	 * Add the Buttons and JPanels needed for the entire keypad to the JFrame
	 */
	private void drawKeypad() {
		JPanel keypad = new JPanel();
		keypad.setLayout(new GridLayout(5,1));
		for(int i = 1; i <= 7; i += 3) {
			JPanel row = new JPanel();
			row.setLayout(new GridLayout(1,3));
			for(int j = 0; j <= 2; j++) {
				JButton temp = new JButton("" + (j+i));
				temp.addActionListener(new ButtonListener());
				row.add(temp);
			}
			keypad.add(row);
		}
		JPanel row = new JPanel();
		row.setLayout(new GridLayout(1,3));
		JButton temp = new JButton("0");
		temp.addActionListener(new ButtonListener());
		row.add(temp);
		temp = new JButton("OK");
		temp.addActionListener(new ButtonListener());
		row.add(temp);
		temp = new JButton("Clear");
		temp.addActionListener(new ButtonListener());
		row.add(temp);
		keypad.add(row);
		row = new JPanel();
		row.setLayout(new GridLayout(1,2));
		temp = new JButton("Cancel");
		temp.addActionListener(new ButtonListener());
		row.add(temp);
		temp = new JButton("Close");
		temp.addActionListener(new ButtonListener());
		row.add(temp);
		keypad.add(row);
		
		main.add(keypad, BorderLayout.EAST);
	}
}
