import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * The GUI for the Bank model.  Displays bank accounts and information and allows
 * for opening ATM windows
 * 
 * @author itp7999, mxd5415
 */
@SuppressWarnings("serial")
public class BankGUI extends JFrame implements Observer, WindowListener {
	
	private Bank model;
	private HashMap<String, JCheckBox> checkBoxes;
	private LinkedList<JFrame> atms;
	private JScrollPane scroll;

	BankGUI(Bank model) {
		this.model = model;
		model.addObserver(this);
		HashMap<String, Account> accounts = model.getAccounts();
		checkBoxes = new HashMap<String, JCheckBox>();
		atms = new LinkedList<JFrame>();
		setLayout(new BorderLayout());
		addWindowListener(this);
		ButtonListener listener = new ButtonListener();
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		
		JButton update = new JButton("Update");
		update.setMargin(new Insets(5, 5, 5, 5));
		update.setSize(50,50);
		update.addActionListener(listener);
		buttonPanel.add(update);
		
		JButton launchATM = new JButton("Open ATM");
		launchATM.setMargin(new Insets(5, 5, 5, 5));
		launchATM.setSize(50,50);
		launchATM.addActionListener(listener);
		buttonPanel.add(launchATM);
		
		JPanel accountsPanel = new JPanel();
		accountsPanel.setLayout(new GridLayout(accounts.size()+1, 1, 3, 3));
		
		JPanel accountInfo = new JPanel();
		accountInfo.setLayout(new GridLayout(1, 4));
		accountInfo.add(new JLabel("Number"));
		accountInfo.add(new JLabel("Type"));
		accountInfo.add(new JLabel("Balance"));
		accountInfo.add(new JLabel("Open ATM?"));
		accountInfo.setBorder(BorderFactory.createLineBorder(Color.black));
		accountsPanel.add(accountInfo);
		
		for(String id : accounts.keySet()) {
			accountInfo = new JPanel();
			accountInfo.setLayout(new GridLayout(1, 4));
			accountInfo.add(new JLabel(id.toString()));
			accountInfo.add(new JLabel(accounts.get(id).getAccountType()));
			accountInfo.add(new JLabel(""+accounts.get(id).getAccountBalance()));
			JCheckBox temp = new JCheckBox();
			checkBoxes.put(id, temp);
			accountInfo.add(temp);
			
			accountInfo.setBorder(BorderFactory.createLineBorder(Color.black));
			accountsPanel.add(accountInfo);
		}

		scroll = new JScrollPane(accountsPanel);
		add(scroll, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		
		setTitle("ACME Bank (Made by Ian Perry & Mitchell Derby)");
		setSize(550, 400);
		setLocation(100, 100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
//			Object button = e.getSource();
			if (e.getActionCommand().compareTo("Update") == 0) {
				update(model, null);
			}
			else if (e.getActionCommand().compareTo("Open ATM") == 0) {
				int num = 0;
				for(String id : checkBoxes.keySet()) {
					if(checkBoxes.get(id).isSelected()) {
						num++;
						atms.add(new AtmGUI(model, id));
					}
				}
				if (num == 0)
					atms.add(new AtmGUI(model, "-1"));
			}
		}
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		int vert = scroll.getVerticalScrollBar().getValue();
		int horiz = scroll.getHorizontalScrollBar().getValue();
		remove(scroll);

		HashMap<String, Account> accounts = model.getAccounts();
		JPanel accountsPanel = new JPanel();
		accountsPanel.setLayout(new GridLayout(accounts.size()+1, 1, 3, 3));
		
		JPanel accountInfo = new JPanel();
		accountInfo.setLayout(new GridLayout(1, 4));
		accountInfo.add(new JLabel("Number"));
		accountInfo.add(new JLabel("Type"));
		accountInfo.add(new JLabel("Balance"));
		accountInfo.add(new JLabel("Open ATM?"));
		accountInfo.setBorder(BorderFactory.createLineBorder(Color.black));
		accountsPanel.add(accountInfo);
		
		for(String id : accounts.keySet()) {
			accountInfo = new JPanel();
			accountInfo.setLayout(new GridLayout(1, 4));
			accountInfo.add(new JLabel(id.toString()));
			accountInfo.add(new JLabel(accounts.get(id).getAccountType()));
			accountInfo.add(new JLabel(""+accounts.get(id).getAccountBalance()));
			JCheckBox temp = new JCheckBox();
			checkBoxes.put(id, temp);
			accountInfo.add(temp);
			
			accountInfo.setBorder(BorderFactory.createLineBorder(Color.black));
			accountsPanel.add(accountInfo);
		}

		scroll = new JScrollPane(accountsPanel);
		scroll.getVerticalScrollBar().setValue(vert);
		scroll.getHorizontalScrollBar().setValue(horiz);
		add(scroll, BorderLayout.CENTER);
		
		repaint();
		validate();
	}
	
//	public static void main(String[] args) {
//		new BankGUI(new Bank());
//	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {
		model.finish();
		for(JFrame f : atms) {
			if(f.isShowing())
				f.dispose();
		}
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

}
