import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Set;

/*
 * Bank.java
 *
 * File:
 *	$Id: 
 *
 * Revisions:
 *	$Log: 
 *
 */

/**
 * The main class. It acts as the model (MVC). It takes one required argument and one optional one.
 * The required argument is the file path to a 'BankFile.txt' that the bank reads initial accounts from and
 * writes final account data to. The optional argument is the file path to a 'BatchMode.txt' that the bank
 * reads in commands from to execute on the accounts in the bank.
 * 
 * @author Mitchell Derby, Ian Perry
 *
 */
public class Bank extends Observable{

	/**
	 * The data structure that holds all of the accounts currently open in the bank. Takes a key, value pair (account ID #, Account object).
	 */
	private HashMap<String,Account> Accounts = new HashMap<String,Account>();
	
	/**
	 * The file corresponding to the given bankFile path argument. Used to read and write initial and final account data to.
	 */
	private File bankFile;
	
	/**
	 * The main method of the bank class. Creates a new bank object. Runs the initialSetup() for that object.
	 * If it was given a second parameter it executes the batch mode.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length > 2){
			System.err.println("Usage: java Bank bankFile [batchFile]");
		}
		
		Bank bank = new Bank();
		bank.initialSetup(args[0]);
		
		if(args.length == 2){
			bank.batchMode(args[1]);
		}
		else
			new BankGUI(bank);
	}
	
	/**
	 * Run at the beginning of every session to read in the starting account data. Reads the data in from a 
	 * file from the given file path.
	 * 
	 * @param filePath - The file path to the BankFile where the initial account data will be read into from.
	 */
	public void initialSetup(String filePath){
		
		System.out.println("========== Initial Data ============");
		System.out.println("");
		System.out.println(" Account Type    Account Balance");
		System.out.println(" ------------    ------- ------------");
		
		try{
			bankFile = new File(filePath);
			BufferedReader read = new BufferedReader(new FileReader(bankFile));
			String nextCommand;
			String[] splitCommand = new String[5];
		
			nextCommand = read.readLine();
			while(nextCommand != null){
				splitCommand = nextCommand.split(" ");
				String accountType = splitCommand[0];
				String ID = splitCommand[1];
				String pin = splitCommand[2];
				double balance = Double.parseDouble(splitCommand[3]);
			
				openAccount(accountType, ID, pin, balance);
						
				nextCommand = read.readLine();
			}
			
			Set<String> keys = Accounts.keySet();
			Iterator<String> iterator = keys.iterator();
			
			while(iterator.hasNext()){
				String key = iterator.next(); 
				String accountType = Accounts.get(key).getAccountType();
				String accountNumber = Accounts.get(key).getID();
				double balance = Accounts.get(key).getAccountBalance();
				System.out.println(accountType+"        "+accountNumber+"        "+balance);
			}
			
			System.out.println("====================================="+"\n");
			read.close();
			
		}catch(FileNotFoundException a){
			try {
				Files.createFile(bankFile.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}catch(IOException a ){
			System.err.print("Unhandled IOException");
		}
	}

	/**
	 * Reads in commands from a given batch file. The first character in each line determines what action is taken:
	 * If the first character is 'a' - Applies interest to all of the accounts.
	 * If the first character is 'o' - Opens a new account using the info provided on the rest of the line.
	 * If the first character is 'd' - Deposits a given amount into a given account.
	 * If the first character is 'w' - Withdraws a given amount into a given account.
	 * If the first character is 'c' - Closes the given account by removing it from the banks list of accounts.
	 * These commands can fail for the following reasons: The given account does not exist (d,w,c), or already exists (o), there may 
	 * be insufficient funds (w).
	 * 
	 * @param filePath - The filePath pointing to the batch file that contains the commands to be executed on the bank.
	 */
	public void batchMode(String filePath){
		String tab = "    ";
		
		try{
			File batchFile = new File(filePath);
			BufferedReader read = new BufferedReader(new FileReader(batchFile));
		
			String nextCommand;
			String[] splitCommand = new String[5];
		
			nextCommand = read.readLine();
			while(nextCommand != null){
				splitCommand = nextCommand.split(" ");
			
				//If o, open a new account using provided data.
				if(splitCommand[0].equals("o")){
				
					//x, c, or s (Checking, CD, or Savings)
					String accountType = splitCommand[1];
					
					//ID or account number of the new account.
					String ID = splitCommand[2];
					
					//The pin of the new account.
					String pin = splitCommand[3];
					
					//The starting balance of the new account.
					double balance = Double.parseDouble(splitCommand[4]);
					
					//prints out the appropriate message (the success for failure of creating the account)
					if(!Accounts.containsKey(ID)){
						if(accountType.equals("x"))
							System.out.println(ID+"    "+"o  x  Open: Success   $  "+balance);
						else if(accountType.equals("s"))
							System.out.println(ID+"    "+"o  s  Open: Success   $  "+balance);
						else if(accountType.equals("c") && balance >= 500)
							System.out.println(ID+"    "+"o  c  Open: Success   $  "+balance);
						else if(accountType.equals("c") && balance < 500)
							System.out.println(ID+"    "+"o  c  Open: Failed");
					}else{
						if(accountType.equals("x"))
							System.out.println(ID+"    "+"o  x  Open: Failed");
						else if(accountType.equals("s"))
							System.out.println(ID+"    "+"o  s  Open: Failed");
						else if(accountType.equals("c"))
							System.out.println(ID+"    "+"o  c  Open: Failed");
					}
					
					//tries to open a new account with the given data.
					openAccount(accountType, ID, pin, balance);
						
					//If c, close the account tied to the given ID number.
				}else if(splitCommand[0].equals("c")){
				
					if(Accounts.containsKey(splitCommand[1])){
						System.out.println(splitCommand[1]+tab+"c"+tab+"Closed: Success   $ "+Accounts.get(splitCommand[1]).getAccountBalance());

						//removes the account tied to the given ID from the list of accounts, effectively closing it.
						Accounts.remove(Accounts.get(splitCommand[1]));
				
					}else
				
					System.out.println(splitCommand[1]+tab+"c"+tab+"Closed: Failed");
				
					//If d, deposit the given amount in to the given account.
				}else if(splitCommand[0].equals("d")){
				
					if(Accounts.containsKey(splitCommand[1])){
					Accounts.get(splitCommand[1]).deposit(Double.parseDouble(splitCommand[2]));
					System.out.println(splitCommand[1]+tab+"d"+tab+"$   "+splitCommand[2]+tab+"$   "+Accounts.get(splitCommand[1]).getAccountBalance());
					}
					//If w, withdraw the given amount from the given account.
				}else if(splitCommand[0].equals("w")){
				
					boolean actionPreformed;
				
					actionPreformed = Accounts.get(splitCommand[1]).withdraw(Double.parseDouble(splitCommand[2]));
					if(actionPreformed){
						System.out.println(splitCommand[1]+tab+"w"+tab+"$   "+splitCommand[2]+tab+"$   "+Accounts.get(splitCommand[1]).getAccountBalance());
					}else
						System.out.println(splitCommand[1]+tab+"w"+tab+"$   "+splitCommand[2]+tab+"Failed");
				
				}else if(splitCommand[0].equals("a")){
				
					Set<String> keys = Accounts.keySet();
					System.out.println();
					System.out.println("============== Interest Report ==============");
					System.out.println("Account Adjustment "+tab+"New Balance");
					System.out.println("------- -----------"+tab+"-----------");
				
					Iterator<String> iterator = keys.iterator();
				
					while(iterator.hasNext()){
						String key = iterator.next(); 
						double interest = Accounts.get(key).applyInterest();
						double penalty = Accounts.get(key).applyPenalty();
						double totalChange = (penalty+interest);
						System.out.println(key+tab+tab+"$"+tab+totalChange+tab+"$"+tab+Accounts.get(key).getAccountBalance());
					}
					System.out.println("==============================================="+ "\n");
				}
			
				nextCommand = read.readLine();
			}
			
			read.close();
		}catch(FileNotFoundException a){
			System.err.println("Usage: java Bank bankFile [batchFile]");
		}catch(Exception a){
			a.printStackTrace();
		}
	}
	
	/**
	 * Method used create a new account and add it to the list of accounts.
	 * 
	 * @param accountType - either 'x', 'c', or 's'. Indicates which type of account needs to be opened.
	 * @param iD - ID or account number of the new account.
	 * @param pin - Pin of the new account.
	 * @param balance - Balance of the new account.
	 * @return
	 */
	public boolean openAccount(String accountType, String iD, String pin, double balance){
		
		if(Accounts.containsKey(iD)){
			
			return false;
		}
		
		if(accountType.equals("x")){
			
			CheckingAccount account = new CheckingAccount(iD, pin, balance);
			Accounts.put(account.getID(), account);
			return true;
			
		}else if(accountType.equals("s")){
			
			SavingsAccount account = new SavingsAccount(iD, pin, balance);
			Accounts.put(account.getID(), account);
			return true;
			
		}else if(accountType.equals("c")){
			try {
				
				CDAccount account = new CDAccount(iD, pin, balance);
				Accounts.put(account.getID(), account);

				
			} catch (MinimumBalanceException e) {
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Called when the bank finishes batch mode or the bank GUI is closed. Saves all of the account data to the bank File and prints
	 * them to standard out.
	 */
	public void finish(){
		
		System.out.println("========== Final Bank Data ============");
		System.out.println("");
		System.out.println("Account Type    Account Balance");
		System.out.println("------------    ------- ------------");
		
		
		Set<String> keys = Accounts.keySet();
		Iterator<String> iterator = keys.iterator();
		try {
			BufferedWriter write = new BufferedWriter(new FileWriter(bankFile, false));
		
			//iterates over the HashMap and writes their data to the file and System.out.
		while(iterator.hasNext()){
			String key = iterator.next(); 
			String accountType = getAccounts().get(key).getAccountType();
			String accountNumber = Accounts.get(key).getID();
			double balance = Accounts.get(key).getAccountBalance();
			String accountPin = Accounts.get(key).getPin();
			String temp;
			if(accountType.equals("Savings Account")){
				 temp = "s";
			}else if(accountType.equals("Checking Account")){ 
				 temp = "x";
			}else if(accountType.equals("CD Account")){ 
				 temp = "c";
			} else {
				 temp = "";
			}	
			
			//writes each account's data on a separate line in the BankFile.txt and writes the data to System.out.
			write.write(temp+" "+accountNumber+" "+accountPin+" "+balance);
			write.newLine();
			System.out.println(accountType+"       "+accountNumber+"       $ "+balance);
		}
		
		//flushes and closed the writer
		write.flush();
		write.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("=========================================");
	}
	
	/**
	 * returns the File object pointing to the bankFile.
	 * @return
	 */
	public File getBankFile(){
		return bankFile;
	}
	
	/**
	 * returns the HashMap of accounts currently stored in the bank.
	 * @return
	 */
	public HashMap<String, Account> getAccounts(){
		return Accounts;
	}
	
	/**
	 * Method for depositing money into an account in the bank
	 * 
	 * @param ID - The ID of the account from which the withdraw is taking place.
	 * @param amount - The amount to be withdrawn.
	 * @return a boolean indicating whether the action succeeded or failed.
	 */
	public boolean withdraw(String ID, double amount){
		boolean succeded = Accounts.get(ID).withdraw(amount);
		setChanged();
		notifyObservers();
		return succeded;

	}
	
	/**
	 * Method for depositing a given amount into a given account. Calles that accounts deposit method.
	 * 
	 * @param ID - The ID of the account to which the deposit is taking place.
	 * @param amount - The amount to be deposited
	 * @return a boolean indicating whether the action succeeded or failed.
	 */
	public boolean deposit(String ID, double amount){
		boolean succeded = Accounts.get(ID).deposit(amount);
		setChanged();
		notifyObservers();
		return succeded;
	}
	
	/**
	 * Method for getting the account balance from the account associated with the given ID. Calls that accounts getAccountBalance method.
	 * 
	 * @param ID - The ID of the account for which the balance inquiry is about.
	 * @return a double that is the balance of the given account.
	 */
	public Double getAccountBalance(String ID){
		return Accounts.get(ID).getAccountBalance();
	}
	
	/**
	 * Checks to see if the give ID is the ID of an account in the bank.
	 * 
	 * @param ID -The ID to be checked
	 * @return true if the ID is valid
	 */
	public boolean IdIsValid(String ID){
		if(Accounts.containsKey(ID)){
			return true;
		}else
			return false;
		
	}
	
	/**
	 * A method to check to see if a given Pin is valid with the account associated with the given ID number.
	 * 
	 * @param ID - ID of the account in question
	 * @param Pin - Pin to be verified
	 * @return true if the Pin is valid.
	 */
	public boolean PinIsValid(String ID, String Pin){
		if(Accounts.containsKey(ID)){
			if(Accounts.get(ID).getPin().equals(Pin)){
				return true;
			}
		}
			return false;
		
	}
}
