/*
 * Account.java
 *
 * File:
 *	$Id: 
 *
 * Revisions:
 *	$Log: 
 *
 */

/**
 * 
 * @author Mitchell Derby and Ian Perry
 *
 */
public class Account {

	/**
	 * 
	 */
	public String accountType;
	
	/**
	 * The current balance of the account.
	 */
	protected double Balance;
	
	/**
	 * A 4-digit integer (security pin).
	 */
	protected String pin;
	
	/**
	 * A unique multi-digit (4 or more digits) integer.
	 */
	protected String accountNumber;
	
	
	/**
	 * constructor for a bank account.
	 * 
	 * @param accountNumber - the unique ID of the account (4 or more digits).
	 * @param pin - the 4 digit security pin of the account.
	 * @param balance - the starting balance of the account.
	 */
	public Account(String accountNumber, String pin, double balance){
		this.accountNumber = accountNumber;
		this.pin = pin;
		Balance = balance;
	}
	
	/**
	 * A getter for the account type;
	 * @return
	 */
	public String getAccountType(){
		return accountType;
	}
	
	synchronized public boolean withdraw(double amountWithdrawn){
		
		if(amountWithdrawn < Balance && amountWithdrawn > 0){
			Balance = Balance - amountWithdrawn;
			return true;
		}else 
			return false;
	}
	
	
	/**
	 * A method for depositing money into an account. You cannot deposit a negative amount.
	 * 
	 * @param amountToDeposit - The amount being deposited into the account.
	 * @return A boolean showing whether the deposit was successfully applied to the account.
	 * 
	 * <dt><b>Preconditions:</b><dd> amountToDeposit cannot be less than or equal to zero.
	 */
	synchronized public boolean deposit(double amountToDeposit){
		
		if(amountToDeposit <= 0)
			return false;
		else{
			Balance = Balance+amountToDeposit;
			return true;
		}
			
	}
	
	
	/**
	 * method for applying interest to accounts. *Not used for every account*
	 */
	synchronized public double applyInterest(){
		return 0;
	}
	
	
	/**
	 * Method for applying a penalty to an account if its balance 
	 * is below the minimum balance required for the specific account type.
	 * 
	 * @return The dollar amount of the applied penalty.
	 */
	synchronized public double applyPenalty(){
		return 0;
		
	}
	
	
	/**
	 * method for getting the current balance of an account.
	 * 
	 * @return  the current balance of the account.
	 */
	synchronized public double getAccountBalance(){
		return Balance;
	}
	
	
	/**
	 * method for getting the pin of an account.
	 * 
	 * @return  the pin of the account.
	 */
	protected String getPin(){
		return pin;
	}
	
	
	/**
	 * method for getting the ID of an account.
	 * 
	 * @return  the ID of the account.
	 */
	protected String getID(){
		return accountNumber;
	}
}
