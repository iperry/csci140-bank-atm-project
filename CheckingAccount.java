/*
 * CheckingAccount.java
 *
 * File:
 *	$Id: 
 *
 * Revisions:
 *	$Log: 
 *
 */

/**
 * 
 * @author Mitchell Derby and Ian Perry
 *
 */
public class CheckingAccount extends Account{

	
	/**
	 * constructor for a bank account.
	 * 
	 * @param accountNumber - the unique ID of the account (4 or more digits).
	 * @param pin - the 4 digit security pin of the account.
	 * @param balance - the starting balance of the account.
	 */
	public CheckingAccount(String accountNumber, String pin, double balance) {
		super(accountNumber, pin, balance);
		accountType = "Checking Account";
	}
	
	
	/**
	 * Method for applying a penalty to a Checking account if its balance 
	 * is below the minimum balance required for this specific account type ($50).
	 * 
	 * @return The dollar amount of the applied penalty.
	 */
	synchronized public double applyPenalty(){
		
		if(Balance > 50.00)
			return 0.00;
		else if(Balance < 50.00 && Balance > 5.00){
			Balance = Balance-5.00;
			return 5.00;
		}else if(Balance < 5.00){
			
			/**
			 * The original Balance. Used to return the penalty that was subtracted from the account.
			 */
			double originalBalance = Balance;
			
			/**
			 * double of balance w/ penalty.
			 */
			double tempBalance;
				
			/**
			 * integer of balance w/ penalty.
			 */
			int unroundedBalance;
				
			/**
			 * difference between double and integer values of balance w/ penalty. Used for rounding.
			 */
			double remainder; 
			
			//Gets the whole balance*100 with penalty applied.
			tempBalance = (Balance*100)*.9;
			
			//Gets the balance*100 with penalty applied but without parts of cents.
			unroundedBalance = (int) ((Balance*100)*.9);
			
			//Gets the parts of cents from applying penalty to see if the number needs rounding.
			remainder = tempBalance-unroundedBalance;
		
			//Checks if the balance needs to be rounded to the nearest cent. 
			if(remainder > .5){
				tempBalance = unroundedBalance+1;
			}else{
				tempBalance = unroundedBalance;
			}
			
			Balance = tempBalance;
			
			return originalBalance-Balance;
		}else
			return 0.00;
		
		
	}
	

}
