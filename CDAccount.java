/*
 * CDAccount.java
 *
 * File:
 *	$Id: 
 *
 * Revisions:
 *	$Log: 
 *
 */

/**
 * 
 * @author Mitchell Derby and Ian Perry
 *
 */
public class CDAccount extends Account{

	
	/**
	 * constructor for a bank account
	 * 
	 * @param accountNumber - the unique ID of the account (4 or more digits).
	 * @param pin - the 4 digit security pin of the account.
	 * @param balance - the starting balance of the account.
	 */
	public CDAccount(String accountNumber, String pin, double balance)  throws MinimumBalanceException{
		
		super(accountNumber, pin, balance);
		if(balance < 500){
			throw new MinimumBalanceException();
		}
		
		accountType = "CD Account";

	}
	
	
	synchronized public boolean withdraw(double amountWithdrawn){
		return true;
	}
	
	/**
	 * Method for applying interest to a CD account. The interest rate for CD accounts is 5%/year.
	 */
	synchronized public double applyInterest(){
		
		/**
		 * double of balance w/ interest.
		 */
		double tempBalance;
		
		/**
		 * integer of balance w/ interest.
		 */
		int unroundedBalance;
		
		/**
		 * difference between double and integer values of balance w/ interest. Used for rounding.
		 */
		double remainder; 
		
	
		tempBalance = (int)(Balance*100);
		
		//Gets the balance*100 with interest applied but without parts of cents.
		unroundedBalance = (int) (tempBalance*(1+.05/12));
				
		//Gets the whole balance*100 with interest applied.
		tempBalance = (tempBalance*(1+.05/12));
		
		//Gets the parts of cents from applying interest to see if the number needs rounding.
		remainder = tempBalance-unroundedBalance;
		
		//Checks if the balance needs to be rounded to the nearest cent. 
		if(remainder > .5)
			tempBalance = (unroundedBalance+1)/100;
		else
			tempBalance = (unroundedBalance)/100;
		
		double oldBalance = Balance;
		Balance = tempBalance;
				
		return Balance-oldBalance;
		
	}
	

}
