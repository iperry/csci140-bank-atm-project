243 OOP: Project 02 README
===============================
(please use the RETURN key to make multiple lines; don't assume autowrap.)

0. Author Information
---------------------

CS Username: mxd5415 and itp7999	Name:  Mitchell Derby and Ian Perry

1. Problem Analysis
---------

Summarize the analysis work you did. 
What new information did it reveal?  How did this help you?
How much time did you spend on problem analysis?

	To analyize the problem we talked through a couple of different design ideas that could solve the problem and the issues with each
	of them. With this anaylsis we determined that the best way to connect the model and view would be by using Observable and 
	Observer. We only spent about 30-45 minutes on problem anaylsis.

2. Design
---------

Explain the design you developed to use and why. What are the major 
components? What connects to what? How much time did you spend on design?
Make sure to clearly show how your design uses the MVC model by
separating the UI "view" code from the back-end game "model".

		For our design we have 4 main components. We have the Bank model which holds and manipulates all of the data for the bank.
	We also have the Account and its subclasses which are used to store data. The last two components are the Bank and ATM GUIs.
	They are used to see the accounts located in the bank and to modify this data by logging into the atm with a ID and pin and withdrawing
	or depositing money. The Bank is connected to the UIs by use of Observable and Observer. The Accounts are used by the Bank to store data 
	and interact directly. The ATM GUI uses action listeners and makes method calls to the Bank model for things such as withdraws and deposits.
	We spent a minimal amount of time on design because much of the design for the project was given through lectures and labs.

3. Implementation and Testing
-------------------

Describe your implementation efforts here; this is the coding and testing.

What did you put into code first?
How did you test it?
How well does the solution work? 
Does it completely solve the problem?
Is anything missing?
How could you do it differently?
How could you improve it?
How much total time would you estimate you worked on the project? 
If you had to do this again, what would you do differently?

		The first thing we coded was the Account parent class and its children classes (Checking, CD and Savings accounts).
	After that we put in the Bank file, starting with the batch mode. This was because it did not depend on any other parts 
	of the project. We tested it by using the main method and passing in command line arguments to test the batch mode and we tested the GUI by going through
	and testing all of the buttons at all of the different stages of the ATM's lifecycle. This also tested the accounts. The solution we implemented for batch 
	mode works fairly well. The only problem we have is that depending on the amount of money in the accounts affects how long the printed lines are and this 
	means the data ends up in different places based on the money in each account. We estimate that we spent about 17-18 hours total on this project. We could 
	improve it by making the Bank and ATM UI more appealing looking. Although there are some issues with the printing, the solution does solve all of the given 
	problems and successfully preforms various actions on the accounts. It also allows for immediate updating of the bank UI displayed data. If we did this 
	project over we would have started the proeject a bit earlier. One thing we are missing are commit comments in the files because we used GIT. The commit
	changes for our repository are located here: https://bitbucket.org/iperry/csci140-bank-atm-project/commits/all.


4. Development Process
-----------------------------

Describe your development process here; this is the overall process.

How much problem analysis did you do before your initial coding effort?
How much design work and thought did you do before your initial coding effort?
How many times did you have to go back to assess the problem?

What did you learn about software development?

	We did almost all of our problem anaylsis before our initial coding effort. We did a bit more between out initial effort and the implementation of 
	the GUIs. We did aboout half of our design work before our initial coding effort and the other half before implementaion of the GUIs. We only had
	to go back and assess the problem about 3-4 times during the entire process. These times allowed us to get good grasps on what the next part of 
	the problem required us to do. Some of the things we learned about software development is that when developing software you need to be flexible
	in order to deal with the problems and challenges that will come up that you don't think about before starting the project. This is why coding 
	practices that allow you to change the workings of one part of the project without it breaking other parts are important. MVC is one example of
	such a coding practice. 
